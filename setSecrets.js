import 'dotenv/config';
import { PutSecretValueCommand, SecretsManagerClient } from "@aws-sdk/client-secrets-manager";

const credentials = {
    region: process.env.AWS_REGION,
    credentials: {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
    }
};


const secretsClient = new SecretsManagerClient(credentials);


let variables = process.argv[2].split(',');

let secretObject = {};

variables.forEach((key) => {
    secretObject[key] = getEnv(key);
});

let params = {
    SecretId: process.env.AWS_SECRET_ID,
    SecretString: JSON.stringify(secretObject)
}

secretsClient.send(new PutSecretValueCommand(params)).then((res) => console.log(res));

function getEnv(envName) {
    return process.env[envName] || '';
}