import 'dotenv/config';
import { GetSecretValueCommand, SecretsManagerClient } from "@aws-sdk/client-secrets-manager";

const credentials = {
    region: process.env.AWS_REGION,
    credentials: {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
    }
};

const secretsClient = new SecretsManagerClient(credentials);

let params = {
    SecretId: process.env.AWS_SECRET_ID
}

secretsClient.send(new GetSecretValueCommand(params)).then((data) => {
    let parsedData = JSON.parse(data.SecretString);
    let str = '';
    for (const key in parsedData) {
        if (Object.hasOwnProperty.call(parsedData, key)) {
            let val = parsedData[key];
            if('string' === typeof val){
                val = `"${val}"`;
            }
            str = `${str}${key}=${val}\n`;
        }
    }
    console.log(str);
});